/*Realizar un programa que imprima en pantalla el tamaño en bytes de las variables:
Int,float,char,long int*/
#include <stdio.h>
void main (void)
{
  printf("\nEl tamanno en bytes de Int es : %d",sizeof(int) );
  printf("\nEl tamanno en bytes de float es : %d",sizeof(float) );
  printf("\nEl tamanno en bytes de char es : %d",sizeof(char) );
  printf("\nEl tamanno en bytes de long int es : %d",sizeof(long int) );
}
